rm gempng/*

jupyter nbconvert --to html --execute --ExecutePreprocessor.timeout=600 zzcorgem.ipynb
jupyter nbconvert --to html --execute --ExecutePreprocessor.timeout=600 zzcorgemr.ipynb

rm zzcorgem.html
rm zzcorgemr.html

cat zzcorhead.html zzcoradgemr.html zzcormid.html gemr.html zzcortail.html > zzcoralert.html

git add .
git commit -m "Update & push from zzcoralert.sh"
git push
